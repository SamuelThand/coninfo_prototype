/**
 * Initiates ConInfo Application
 */
function initApp() {
  let inmates = {};
  let clickedListItemNum;

  /**
 * Page function for the index.html
 */
  function indexPage() {
    const loginButton = document.querySelector('#login-button');
    const passField = document.getElementById('password');
    const nameField = document.getElementById('username');
    // User data structure, temporary in place of the database
    const users = {
      0: {
        username: 'user1',
        password: 'pass1',
        mgmtStaff: false,
      },
      1: {
        username: 'user2',
        password: 'pass2',
        mgmtStaff: true,
      },
      2: {
        username: 'testuser',
        password: 'Test1',
        mgmtStaff: false,
      },
    };

    // Disables functionality to use return button to return back into the program after logging out
    // window.history.pushState(null, null, window.location.href);
    // window.onpopstate = () => {
    //   window.history.go(1);
    // };

    // Checks login credentials - temporary in place of a real security solution
    function loginFunction() {
      let counter = 1;
      // eslint-disable-next-line no-restricted-syntax
      for (const [key] of Object.entries(users)) {
        if (users[key].username === nameField.value && users[key].password === passField.value) {
          window.location = ('inmatelist.html');
          break;
        } else if (counter === Object.entries(users).length) {
          alert('Oops, invalid username or password');
          location.reload();
        }
        counter += 1;
      }
    }
    loginButton.addEventListener('click', loginFunction);
  }
  /**
 * Page function for inmatelist.html
 */
  function inmateListpage() {
    const logoutButton = document.querySelector('#logout-button');
    const addnewinmateButton = document.querySelector('#add-new-inmate-button');
    const inmateListSearchField = document.getElementById('search-field');
    let listEntries;
    let totalEntry;

    // Iterates through inmates object and populates the inmate list with the entries
    function populateList() {
      const inmateListSection = document.querySelector('.inmate-list');
      Object.entries(inmates).forEach((property) => {
        const inmateDiv = document.createElement('div');
        inmateDiv.className = `list-entry ${property[0]}`;
        inmateDiv.innerHTML = `${property[1].id} | ${property[1].name}`;
        inmateListSection.append(inmateDiv);
      });
      listEntries = document.querySelectorAll('.list-entry');
    }
    function logoutFunction() {
      window.location = ('index.html');
    }

    function gotoAddnewinmate() {
      window.location = ('addinmate.html');
    }

    function gotoInmateview(Event) {
      // Same number as the key in the inmatesobject
      clickedListItemNum = parseInt(Event.target.classList[1], 10);
      sessionStorage.setItem('clickedListItem', clickedListItemNum);
      window.location = ('inmateview.html');
    }

    function searchFunction() {
      const filter = inmateListSearchField.value.toUpperCase();
      for (let i = 0; i < totalEntry.length; i += 1) {
        const textValue = totalEntry[i].innerHTML;
        if (textValue.toUpperCase().indexOf(filter) > -1) {
          totalEntry[i].style.display = '';
        } else {
          totalEntry[i].style.display = 'none';
        }
      }
    }

    // Updates inmates from session storage, if its not empty
    if (sessionStorage.getItem('inmatesObject')) {
      inmates = JSON.parse(sessionStorage.getItem('inmatesObject'));
    }
    populateList();
    addnewinmateButton.addEventListener('click', gotoAddnewinmate);
    logoutButton.addEventListener('click', logoutFunction);
    listEntries.forEach((Element) => {
      Element.addEventListener('click', gotoInmateview);
    });
    totalEntry = document.querySelectorAll('.list-entry');
    inmateListSearchField.addEventListener('change', searchFunction);
  }

  /**
 * Page function for inmateview.html
 */
  function inmateviewPage() {
    const editButton = document.querySelector('#edit-button');
    const infoFields = document.querySelectorAll('.info-field');
    const uploadNewImageButton = document.querySelector('#upload-new-image-button');
    const saveButton = document.querySelector('#save-button');
    const backButton = document.querySelector('#back-button');
    const delButton = document.querySelector('#delete-button');

    // Disables editing
    function readMode() {
      infoFields.forEach((Element) => {
        Element.setAttribute('disabled', '');
      });
      uploadNewImageButton.setAttribute('hidden', '');
      saveButton.setAttribute('hidden', '');
      editButton.removeAttribute('hidden', '');
    }

    // Enables editing
    function editMode() {
      infoFields.forEach((Element) => {
        Element.removeAttribute('disabled', '');
      });
      uploadNewImageButton.removeAttribute('hidden', '');
      saveButton.removeAttribute('hidden', '');
      editButton.setAttribute('hidden', '');
    }

    // Populates info-fields from the inmates object
    function populateInfo() {
      clickedListItemNum = parseInt(sessionStorage.getItem('clickedListItem'), 10);
      document.querySelector('#name-no').textContent = `${inmates[clickedListItemNum].id} | ${inmates[clickedListItemNum].name}`;
      document.querySelector('#name').value = inmates[clickedListItemNum].name;
      document.querySelector('#inmate-id').value = inmates[clickedListItemNum].id;
      document.querySelector('#ssn').value = inmates[clickedListItemNum].ssn;
      document.querySelector('#cell').value = inmates[clickedListItemNum].cell;
      document.querySelector('#block').value = inmates[clickedListItemNum].block;
      document.querySelector('#emergency-contact').value = inmates[clickedListItemNum].emergencyContact;
      document.querySelector('#last-known-adress').value = inmates[clickedListItemNum].lastKnownAddress;
      document.querySelector('#notes').value = inmates[clickedListItemNum].notes;
    }

    // Updates inmate data structure with edited information, and updates session storage
    function saveInfo() {
      if (confirm('Are you sure you want to save this inmate?')) {
        inmates[clickedListItemNum] = {
          name: `${document.querySelector('#name').value}`,
          id: `${document.querySelector('#inmate-id').value}`,
          ssn: `${document.querySelector('#ssn').value}`,
          cell: `${document.querySelector('#cell').value}`,
          block: `${document.querySelector('#block').value}`,
          emergencyContact: `${document.querySelector('#emergency-contact').value}`,
          lastKnownAddress: `${document.querySelector('#last-known-adress').value}`,
          notes: `${document.querySelector('#notes').value}`,
        };
        // Sends updated JS object inmates to session storage
        sessionStorage.setItem('inmatesObject', JSON.stringify(inmates));
        readMode();
      } else {
        alert('Nothing was saved');
      }
    }

    // Deletes entry from the inmate data strucutre, and updates session storage
    function deleteInmate() {
      if (confirm('Are you sure you want to delete inmate?')) {
        delete inmates[clickedListItemNum];
        // Sends updated JS object inmates to session storage
        sessionStorage.setItem('inmatesObject', JSON.stringify(inmates));
        window.location = ('inmatelist.html');
      } else {
        alert('Inmate was not deleted');
      }
    }

    function goBacktoInmateList() {
      window.location = ('inmatelist.html');
    }

    // Requires server side language - Would be functionality
    function imageUpload() {
      console.log('Upload');
    }

    if (sessionStorage.getItem('inmatesObject')) {
      inmates = JSON.parse(sessionStorage.getItem('inmatesObject'));
    }
    populateInfo();
    readMode();
    editButton.addEventListener('click', editMode);
    uploadNewImageButton.addEventListener('click', imageUpload);
    backButton.addEventListener('click', goBacktoInmateList);
    saveButton.addEventListener('click', saveInfo);
    delButton.addEventListener('click', deleteInmate);
  }

  /**
 * Page function for addinmate.html
 */
  function addinmatePage() {
    const backButton = document.querySelector('#back-button');
    const saveButton = document.querySelector('#save-button');
    const uploadNewImageButton = document.querySelector('#upload-new-image-button');

    function goBacktoInmateList() {
      window.location = ('inmatelist.html');
    }

    // Since we (in the future) are using a database
    // which applies unique id via auto increment function
    // We do not have a solution here for ensuring unique id. The id will be added by the database.
    //
    // Saves the new inmate
    function saveFunction() {
      const inmatesCount = Object.keys(inmates).length;
      let nextInmateNumb;

      // If dictionary is empty, set next inmate key to 0, else to highest key + 1
      if (inmatesCount === 0) {
        nextInmateNumb = inmatesCount;
      } else {
        nextInmateNumb = parseInt(Math.max(...Object.keys(inmates)) + 1, 10);
      }

      if (confirm('Are you sure you want to add the inmate?')) {
      // Adds a new inmate with the self-incremented key to the JS object inmates
        inmates[nextInmateNumb] = {
          name: `${document.querySelector('#name').value}`,
          id: `${document.querySelector('#inmate-id').value}`,
          ssn: `${document.querySelector('#ssn').value}`,
          cell: `${document.querySelector('#cell').value}`,
          block: `${document.querySelector('#block').value}`,
          emergencyContact: `${document.querySelector('#emergency-contact').value}`,
          lastKnownAddress: `${document.querySelector('#last-known-adress').value}`,
          notes: `${document.querySelector('#notes').value}`,
        };

        // Sends updated JS object inmates to session storage
        sessionStorage.setItem('inmatesObject', JSON.stringify(inmates));
        window.location = ('inmatelist.html');
      } else {
        alert('Was not added');
      }
    }

    // Requires server side language
    function uploadFunction() {
      console.log('upload');
    }

    if (sessionStorage.getItem('inmatesObject')) {
      inmates = JSON.parse(sessionStorage.getItem('inmatesObject'));
    }
    backButton.addEventListener('click', goBacktoInmateList);
    saveButton.addEventListener('click', saveFunction);
    uploadNewImageButton.addEventListener('click', uploadFunction);
  }

  /**
 * Pagedetection
 */
  function pageDetection() {
    const path = window.location.pathname;
    if (path.endsWith('inmatelist.html')) {
      inmateListpage();
    } else if (path.endsWith('inmateview.html')) {
      inmateviewPage();
    } else if (path.endsWith('addinmate.html')) {
      addinmatePage();
    } else {
      indexPage();
    }
  }
  window.addEventListener('load', pageDetection);
}

initApp();
