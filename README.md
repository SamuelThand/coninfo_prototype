## Simulated services

- The JS-object 'users' under indexPage() is used to simulate a server side database with user credentials.
- The JS-object 'inmates' simulates the database information about inmate
- Sessionstorage is used to simulate persistance, which would be done from the serverside nodeJS + database  

## Working functionalities

- Adding of inmates
- Deletion of inmates
- Inmate searching 
- Inmate editing
- Responsive design
